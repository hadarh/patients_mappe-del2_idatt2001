package idatt2001.mappeDel2.patients.backend;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    Patient patient1;
    Patient patient2;
    Patient patient1Copy;

    @BeforeEach
    void setUp() {
        patient1 = new Patient("Ole", "Olsen", "12345678910", "Covid-19", "Per Peterson");
        patient2 = new Patient("Nils", "Nilsen", "10987654321", "Ebola", "Karl Peterson");
        patient1Copy = new Patient("Ole", "Olsen", "12345678910", "Covid-19", "Per Peterson");
    }

    @Test
    void addPatientTest() {
        PatientRegister.getPatientsInstance().addPatient(patient1);
        PatientRegister.getPatientsInstance().addPatient(patient2);
        assertEquals(2, PatientRegister.getPatientsInstance().getPatients().size());
        assertTrue(PatientRegister.getPatientsInstance().getPatients().contains(patient1) &&
                PatientRegister.getPatientsInstance().getPatients().contains(patient2));

        PatientRegister.getPatientsInstance().getPatients().clear();
    }

    @Test
    void editPatientTest() {
        PatientRegister.getPatientsInstance().getPatients().add(patient1);
        PatientRegister.getPatientsInstance().editPatient( patient1, patient2);

        assertTrue(patient1.getFirstName().equals(patient2.getFirstName()) && patient1.getLastName().equals(patient2.getLastName())
        && patient1.getSocialSecurityNumber().equals(patient2.getSocialSecurityNumber()) && patient1.getDiagnosis().equals(patient2.getDiagnosis())
        && patient1.getGeneralPractitioner().equals(patient2.getGeneralPractitioner()));

        assertFalse(patient1.getFirstName().equals(patient1Copy.getFirstName()) && patient1.getLastName().equals(patient1Copy.getLastName())
                && patient1.getSocialSecurityNumber().equals(patient1Copy.getSocialSecurityNumber()) && patient1.getDiagnosis().equals(patient1Copy.getDiagnosis())
                && patient1.getGeneralPractitioner().equals(patient1Copy.getGeneralPractitioner()));

        PatientRegister.getPatientsInstance().getPatients().clear();
    }

    @Test
    void removePatientTest() {
        PatientRegister.getPatientsInstance().getPatients().add(patient1);
        assertTrue(PatientRegister.getPatientsInstance().getPatients().contains(patient1));
        assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());
        PatientRegister.getPatientsInstance().removePatient(patient1);
        assertFalse(PatientRegister.getPatientsInstance().getPatients().contains(patient1));
        assertEquals(0, PatientRegister.getPatientsInstance().getPatients().size());


        PatientRegister.getPatientsInstance().getPatients().clear();
    }


    //Testing handling negative results

    @Test
    void addExistingPatientTest() {
        PatientRegister.getPatientsInstance().addPatient(patient1);
        assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());
        assertTrue(PatientRegister.getPatientsInstance().getPatients().contains(patient1));

        try {
            PatientRegister.getPatientsInstance().addPatient(patient1);
        }catch (IllegalArgumentException e){
            assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());
        }

        PatientRegister.getPatientsInstance().getPatients().clear();
    }
    @Test
    void editNonExistingPatientTest() {
        PatientRegister.getPatientsInstance().getPatients().add(patient1);
        assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());

        try{
            PatientRegister.getPatientsInstance().editPatient( patient2, patient1);
        }catch(IllegalArgumentException e){
            assertFalse(patient2.getFirstName().equals(patient1.getFirstName()) && patient2.getLastName().equals(patient1.getLastName())
                    && patient2.getSocialSecurityNumber().equals(patient1.getSocialSecurityNumber()) && patient2.getDiagnosis().equals(patient1.getDiagnosis())
                    && patient2.getGeneralPractitioner().equals(patient1.getGeneralPractitioner()));
        }

        PatientRegister.getPatientsInstance().getPatients().clear();
    }

    @Test
    void removeNonExistingPatientTest() {
        PatientRegister.getPatientsInstance().getPatients().add(patient1);
        assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());

        try{
            PatientRegister.getPatientsInstance().removePatient(patient2);
        }catch(IllegalArgumentException e){
            assertEquals(1, PatientRegister.getPatientsInstance().getPatients().size());
        }

        PatientRegister.getPatientsInstance().getPatients().clear();
    }
}