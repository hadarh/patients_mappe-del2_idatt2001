module idatt2001.mappeDel2.patients {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires java.sql;

    exports idatt2001.mappeDel2.patients;
    opens idatt2001.mappeDel2.patients.backend;
    opens idatt2001.mappeDel2.patients.alertBox;
    opens idatt2001.mappeDel2.patients.factoryPattern;
    opens idatt2001.mappeDel2.patients.controllers to javafx.fxml;
    opens idatt2001.mappeDel2.patients to javafx.fxml;

}