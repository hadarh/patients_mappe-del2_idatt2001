package idatt2001.mappeDel2.patients.controllers;

import idatt2001.mappeDel2.patients.backend.Patient;
import idatt2001.mappeDel2.patients.backend.PatientRegister;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Class AddEditPatientController
 * @version 1.01 2021-04-30
 * @author Hadar Hayat
 */
public class AddEditPatientController {

    @FXML
    private TextField firstnameInput;
    @FXML
    private TextField lastnameInput;
    @FXML
    private TextField socialsecuritynumberInput;
    @FXML
    private TextArea diagnosisInput;
    @FXML
    private TextField generalpractitionerInput;
    @FXML
    private Button saveButton;

    /**
     * Acts as an eventHandler for the save button
     * Runs either addPatient or editPatient methods depending on the functionality
     * Updates table in primary controller
     */
    public void onButtonAction(boolean isEdit, Patient patient){
        saveButton.setOnAction(actionEvent -> {
            if(isEdit){
                editPatient(patient);
            }else{
                addPatient();
            }
            Stage stage = (Stage) saveButton.getScene().getWindow();
            stage.close();
            PatientRegister.getPatientsInstance().getPrimaryController().populateTable();
        });
    }

    /**
     * Fills all the input fields in the form (addEditPatient.fxml file) with the details about a certain patient
     *
     * @param patient
     */
    public void setPatient(Patient patient) {
        if (patient != null){
            if (firstnameInput != null) firstnameInput.setText(patient.getFirstName());
            if (lastnameInput != null) lastnameInput.setText(patient.getLastName());
            if (socialsecuritynumberInput != null) socialsecuritynumberInput.setText(patient.getSocialSecurityNumber());
            if (diagnosisInput != null) diagnosisInput.setText(patient.getDiagnosis());
            if (generalpractitionerInput != null) generalpractitionerInput.setText(patient.getGeneralPractitioner());
        }
    }

    /**
     * Add a new patient to the Patient Register with the data from the input fields in the form (addEditPatient.fxml file)
     * Updates the status label indicating a new patient has been added
     */
    public void addPatient(){
        if (firstnameInput.getText().isBlank() || lastnameInput.getText().isBlank() || socialsecuritynumberInput.getText().isBlank()) {
            showError("Missing required info", "You have left out some required information about the patient. \n");
        } else {
        PatientRegister.getPatientsInstance().addPatient(new Patient(firstnameInput.getText(), lastnameInput.getText(),
                socialsecuritynumberInput.getText(), diagnosisInput.getText(), generalpractitionerInput.getText()));
        PatientRegister.getPatientsInstance().getPrimaryController().setStatusOutputText("Patient successfully added");
        }
    }

    /**
     * Edits the details of a patient selected from the Patient Register with the data from the input fields in the form (addEditPatient.fxml file)
     * Updates the status label indicating a patient has been updated
     *
     * @param patient
     */
    public void editPatient(Patient patient){
        if (firstnameInput.getText().isBlank() || lastnameInput.getText().isBlank() || socialsecuritynumberInput.getText().isBlank()) {
            showError("Missing required info", "You have left out some required information about the patient. \n");
        } else {
        PatientRegister.getPatientsInstance().editPatient(patient, new Patient(firstnameInput.getText(), lastnameInput.getText(),
                socialsecuritynumberInput.getText(), diagnosisInput.getText(), generalpractitionerInput.getText()));
        PatientRegister.getPatientsInstance().getPrimaryController().setStatusOutputText("Patient info successfully updated");
        }
    }

    /**
     * Pops up Warning Alert to notify user that the patient to be added or edited is missing required information
     */
    public void showError(String headerText, String contentText){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }
}
