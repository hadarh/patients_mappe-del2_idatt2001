package idatt2001.mappeDel2.patients.controllers;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import idatt2001.mappeDel2.patients.App;
import idatt2001.mappeDel2.patients.alertBox.SelectConfirmation;
import idatt2001.mappeDel2.patients.backend.Patient;
import idatt2001.mappeDel2.patients.backend.PatientRegister;
import idatt2001.mappeDel2.patients.backend.TestData;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Class PrimaryController
 * @version 1.01 2021-04-30
 * @author Hadar Hayat
 */
public class PrimaryController {

    private static final String VERSION = "1.0.1";
    private static final String DATE = "2021/04/29";

    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:~/patients;create=true";

    @FXML
    private TableView<Patient> patientsTable;
    @FXML
    private Button addButton;
    @FXML
    private Button editButton;
    @FXML
    private Button removeButton;
    @FXML
    private MenuItem addPatientMenuItem;
    @FXML
    private MenuItem editPatientMenuItem;
    @FXML
    private MenuItem removePatientMenuItem;
    @FXML
    private Label statusOutput;

    private static Stage stage;
    private static Scene scene;

    /**
     * Contains all the methods that must run when the primary.fxml file is loaded/initialized
     */
    public void initialize() {
        PatientRegister.getPatientsInstance().setPrimaryController(this);
        //TestData.generate();
        createTable();
        populateTable();
        onButtonAction();
    }

    /**
     * Acts as an eventHandler for the add, edit and remove buttons and menu items
     * Updates the status label if when a patient is removed
     */
    public void onButtonAction(){
        EventHandler<ActionEvent> addEvent = (actionEvent -> {
            triggerAddEditPatient(false, null);
            populateTable();
        });
        addButton.setOnAction(addEvent);
        addPatientMenuItem.setOnAction(addEvent);

        EventHandler<ActionEvent> editEvent = (actionEvent -> {
            Patient selectedPatient = patientsTable.getSelectionModel().getSelectedItem();
            triggerAddEditPatient(true, selectedPatient);
            populateTable();
        });
        editButton.setOnAction(editEvent);
        editPatientMenuItem.setOnAction(editEvent);

        EventHandler<ActionEvent> removeEvent = (actionEvent -> {
            Patient selectedPatient = patientsTable.getSelectionModel().getSelectedItem();
            triggerRemovePatient(selectedPatient);
            populateTable();
            setStatusOutputText("Patient successfully deleted");
        });
        removeButton.setOnAction(removeEvent);
        removePatientMenuItem.setOnAction(removeEvent);
    }

    /**
     * Creates and adds all necessary columns to the table present in primary.fxml
     * Sets the cellFactoryValue for each column
     */
    public void createTable(){
        patientsTable.getColumns().clear();
        TableColumn<Patient, String> firstNameColumn
                = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> lastNameColumn
                = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, String> socialSecurityNumberColumn
                = new TableColumn<>("Social Security Number");
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        TableColumn<Patient, String> diagnosisColumn
                = new TableColumn<>("Diagnosis");
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        diagnosisColumn.setVisible(false);

        TableColumn<Patient, String> generalPractitionerColumn
                = new TableColumn<>("General Practitioner");
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        generalPractitionerColumn.setVisible(false);

        patientsTable.getColumns().addAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn, diagnosisColumn, generalPractitionerColumn);
    }

    /**
     * Removes all content from patients table
     * Adds the content back to populate the table with the updated list from the patients register
     */
    public void populateTable(){
        patientsTable.getItems().clear();
        patientsTable.setItems(PatientRegister.getPatientsInstance().getAsObservableList());
    }

    /**
     * Loads addEditPatient.fxml on a new stage
     * Sets the title to either Add Patient or Edit Patient depending on the functionality
     * Displays empty form if functionality is Add Patient
     * Displays form with selected patient details if functionality is Edit Patient
     * Shows the new stage
     * Calls the method to either Add or Edit a patient in the AddEditPatientController
     *
     * @param isEdit
     * @param selectedPatient
     */
    public void triggerAddEditPatient(Boolean isEdit, Patient selectedPatient){
        try {
            stage = new Stage();
            FXMLLoader loader = new FXMLLoader(App.class.getResource("addEditPatient.fxml"));
            scene = new Scene(loader.load(), 500, 500);
            stage.setScene(scene);
            AddEditPatientController controller = loader.getController();

            if(isEdit){
                if (selectedPatient == null) {
                    showUnselectedPatientError();
                    return;
                }else{
                stage.setTitle("Edit Patient");
                controller.setPatient(selectedPatient);}

            }else if (!isEdit) stage.setTitle("Add Patient");

            controller.onButtonAction(isEdit, selectedPatient);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Runs the removePatient method in the PatientRegister to remove the selected patient
     * If there is no selected patient it runs method to show an alert message
     *
     * @param selectedPatient
     */
    public void triggerRemovePatient(Patient selectedPatient){
        if (selectedPatient == null) {
            showUnselectedPatientError();
        } else {
            if (showDeleteConfirm()) {
                PatientRegister.getPatientsInstance().removePatient(selectedPatient);
            }
        }
    }

    /**
     * Pops up an Information Alert to display information about the application
     */
    public void showAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Patient Register\nVersion:   " + VERSION);
        alert.setContentText("A application created by\n" + "(C)Hadar Hayat\n" + DATE);

        alert.showAndWait();
    }

    /**
     * Pops up a Confirmation Alert to either confirm or cancel the removal of a patient from the Patient Register
     */
    public boolean showDeleteConfirm(){
        boolean success = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Patient delete confirmation");
        alert.setContentText("Are you sure you want to delete the selected patient?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()) {
            success = (result.get() == ButtonType.OK);
        }
        return success;
    }

    /**
     * Pops up a Warning Alert to notify the user that a patient must be selected to run the selected method
     */
    public void showUnselectedPatientError(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error");
        alert.setHeaderText("No patient selected");
        alert.setContentText("You have not selected a patient from the table. \n" +
                "Please select a patient from the table.");

        alert.showAndWait();
    }

    /**
     * Pops up a Confirmation Alert to either confirm to exit the application or cancel
     */
    public void exitApp() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit");
        alert.setHeaderText("Exit Application");
        alert.setContentText("Are you sure you want to exit the patient register?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && (result.get() == ButtonType.OK)) {
            Platform.exit();
        }
    }

    /**
     * Sets the text on the status label at the bottom of the application interface
     *
     * @param text
     */
    public void setStatusOutputText(String text) {
        statusOutput.setText(text);
    }

    /**
     * Exports the data in the Patient Register to as a .csv file
     * Saves the file with a file name and directory location chosen by the user
     * Updates the status label with the status of the export
     */
    public void exportCSV() throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export patient register");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));

        Writer writer = null;
        try {
            File file = fileChooser.showSaveDialog(stage);
            writer = new BufferedWriter(new FileWriter(file));
            for (Patient patient : PatientRegister.getPatientsInstance().getPatients()) {

                String patientDetails = patient.getFirstName() + ";" + patient.getLastName() + ";" +
                        patient.getGeneralPractitioner() + ";" + patient.getSocialSecurityNumber() +"\n";

                writer.write(patientDetails);
            }
        } catch (Exception ex) {
            setStatusOutputText("Register export failed");
        }
        finally {

            writer.flush();
            writer.close();
            populateTable();
            setStatusOutputText("Register successfully exported");
        }
    }


    /**
     * Imports data into the Patient Register from a .csv file chosen by the user
     * Updates the status label with the status of the import
     */
    public void importCSV(){
        PatientRegister.getPatientsInstance().getPatients().clear();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import patient register");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
            File file = fileChooser.showOpenDialog(stage);

            if(file == null){
                setStatusOutputText("Register import failed");
                if(SelectConfirmation.display()){
                    importCSV();
                    return;
                }else return;
            }

        String FieldDelimiter = ";";

        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(file));

            String line;
            while ((line = br.readLine()) != null) {
                String[] fields = line.split(FieldDelimiter, -1);

                Patient patient = new Patient(fields[0], fields[1], fields[3],
                        null, fields[2]);
                PatientRegister.getPatientsInstance().getPatients().add(patient);
            }
            populateTable();
            setStatusOutputText("Register successfully imported");
        } catch (FileNotFoundException e) {
            setStatusOutputText("Register import failed");
            e.printStackTrace();
        } catch (IOException e) {
            setStatusOutputText("Register import failed");
            e.printStackTrace();
        }
    }

}
