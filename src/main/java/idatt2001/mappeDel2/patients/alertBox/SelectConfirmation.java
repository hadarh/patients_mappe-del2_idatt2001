package idatt2001.mappeDel2.patients.alertBox;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Creates a pop up window
 */
public class SelectConfirmation {

    static boolean response;

    public static boolean display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL); //block input events until handled
        window.setTitle("Error");
        window.setMinWidth(500);
        window.setMinHeight(180);

        Label label = new Label();
        label.setText("The chosen file type is not valid.\n" +
                "Please choose another file by clicking on Select File or cancel the operation");

        Button confirmButton = new Button("Select file");
        confirmButton.getStyleClass().add("confirmButton");
        confirmButton.setOnAction(e -> {
            response = true;
            window.close();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.getStyleClass().add("cancelButton");
        cancelButton.setOnAction(event -> {
            response = false;
            window.close();
        });

        HBox buttonBox = new HBox(10);
        buttonBox.getChildren().addAll(confirmButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, buttonBox);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return response;
    }
}