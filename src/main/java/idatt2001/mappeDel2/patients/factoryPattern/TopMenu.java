package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.scene.Node;
import javafx.scene.control.MenuBar;
import  javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
/**
 * Class TopMenu
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class TopMenu implements GUIObject{

    private MenuBar mainMenuBar;
    private Menu file;
    private Menu edit;
    private Menu help;

    @Override
    /**
     * Build method from GUIObject Interface
     * Builds the menu at top, to chose the different features in the app
     */
    public Node build(){
        file = new Menu();
        file.setText("File");
        MenuItem csvImport = new MenuItem("Import .csv file");
        MenuItem csvExport = new MenuItem("Export .csv file");
        MenuItem exit = new MenuItem("Exit");
        file.getItems().addAll(csvImport, csvExport, exit);

        edit = new Menu();
        edit.setText("Edit");
        MenuItem addPatient = new MenuItem("Add new patient");
        MenuItem editPatient = new MenuItem("Edit selected patient");
        MenuItem removePatient = new MenuItem("Remove selected patient");
        edit.getItems().addAll(addPatient, editPatient, removePatient);

        help = new Menu();
        help.setText("Help");
        MenuItem about = new MenuItem("About");
        help.getItems().addAll(about);

        mainMenuBar = new MenuBar();
        mainMenuBar.getMenus().addAll(file, edit, help);

        return mainMenuBar;
    }

}
