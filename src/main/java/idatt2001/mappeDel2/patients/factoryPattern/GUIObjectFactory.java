package idatt2001.mappeDel2.patients.factoryPattern;

/**
 * Class GUIObjectFactory
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class GUIObjectFactory {

    /**
     * Method to call all the different objects built
     */
    public static GUIObject getGUIObject(String objectType) {
        if (objectType == null) {
            return null;
        }
        if (objectType.equalsIgnoreCase("TOPMENU")) {
            return new TopMenu();

        } else if (objectType.equalsIgnoreCase("TOOLBAR")) {
            return new Toolbar();

        } else if (objectType.equalsIgnoreCase("TABLE")) {
            return new Table();

        }else if (objectType.equalsIgnoreCase("STATUSBAR")) {
            return new StatusBar();

        }else if (objectType.equalsIgnoreCase("DETAILSFORM")) {
            return new DetailsForm();
        }

        return null;
    }
}
