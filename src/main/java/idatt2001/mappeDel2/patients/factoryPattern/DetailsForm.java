package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Class DetailsForm
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class DetailsForm implements GUIObject{

    private Label title;

    private HBox hbox1;
    private Label firstNameLabel;
    private TextField firstNameInput;

    private HBox hbox2;
    private Label lastNameLabel;
    private TextField lastNameInput;

    private HBox hbox3;
    private Label socialSecurityNumberLabel;
    private TextField socialSecurityNumberInput;

    private HBox hbox4;
    private Label diagnosisLabel;
    private TextArea diagnosisInput;

    private HBox hbox5;
    private Label generalPractitionerLabel;
    private TextField generalPractitionerInput;

    private Button saveButton;

    private VBox fullContainer;

    /**
     * Build method from GUIObject Interface
     * Builds the form for the patient input and edit
     */
    @Override
    public Node build(){

        title = new Label("Add/Edit patient details");

        firstNameLabel = new Label("First Name:");
        firstNameInput = new TextField();
        firstNameInput.setPromptText("Ole");
        hbox1 = new HBox();
        hbox1.getChildren().addAll(firstNameLabel, firstNameInput);
        hbox1.setSpacing(12);

        lastNameLabel = new Label("Last Name:");
        lastNameInput = new TextField();
        lastNameInput.setPromptText("Olsen");
        hbox2 = new HBox();
        hbox2.getChildren().addAll(lastNameLabel, lastNameInput);
        hbox2.setSpacing(12);

        socialSecurityNumberLabel = new Label("Social Security Number:");
        socialSecurityNumberInput = new TextField();
        socialSecurityNumberInput.setPromptText("DDMMYYYYXXXXX");
        hbox3 = new HBox();
        hbox3.getChildren().addAll(socialSecurityNumberLabel, socialSecurityNumberInput);
        hbox3.setSpacing(12);

        diagnosisLabel = new Label("Diagnosis:");
        diagnosisInput = new TextArea();
        hbox4 = new HBox();
        hbox4.getChildren().addAll(diagnosisLabel, diagnosisInput);
        hbox4.setSpacing(12);

        generalPractitionerLabel = new Label("General Practitioner:");
        generalPractitionerInput = new TextField();
        generalPractitionerInput.setPromptText("Per Olsen");
        hbox5 = new HBox();
        hbox5.getChildren().addAll(generalPractitionerLabel, generalPractitionerInput);
        hbox5.setSpacing(12);

        saveButton = new Button("Save");

        fullContainer = new VBox();
        fullContainer.getChildren().addAll(title, hbox1, hbox2, hbox3, hbox4, hbox5, saveButton);
        fullContainer.setSpacing(20);

        return fullContainer;
    }

}
