package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.scene.Node;

/**
 * Interface GUIObject
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public interface GUIObject {
    /**
     * Method in which all the different objects will be built
     */
    Node build();
}
