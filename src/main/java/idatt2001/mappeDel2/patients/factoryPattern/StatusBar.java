package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * Class StatusBar
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class StatusBar implements GUIObject{

    private HBox labelContainer;
    private Label statusLabel;
    private Label variableStatusLabel;


    @Override
    /**
     * Build method from GUIObject Interface
     * Builds the status bar to show the current status of the app
     */
    public Node build(){
        statusLabel = new Label();
        statusLabel.setText("Status:");

        variableStatusLabel = new Label();
        variableStatusLabel.setText("temp");

        labelContainer = new HBox();
        labelContainer.getChildren().addAll(statusLabel, variableStatusLabel);
        labelContainer.setSpacing(12);
        labelContainer.setPadding(new Insets(10));

        return labelContainer;
    }

}
