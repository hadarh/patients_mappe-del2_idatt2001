package idatt2001.mappeDel2.patients.factoryPattern;

import idatt2001.mappeDel2.patients.backend.Patient;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Class Table
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class Table implements GUIObject{

    private TableView patientsTable;
    private TableColumn<Patient, String> firstNameColumn;
    private TableColumn<Patient, String> lastNameColumn;
    private TableColumn<Patient, String> socialSecurityNumberColumn;
    private TableColumn<Patient, String> diagnosisColumn;
    private TableColumn<Patient, String> generalPractitionerColumn;

    @Override
    /**
     * Build method from GUIObject Interface
     * Builds the table containing the patient details
     */
    public Node build(){
        patientsTable = new TableView<Patient>();

        firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setMinWidth(200);

        lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setMinWidth(200);

        socialSecurityNumberColumn = new TableColumn<>("Social Security Number");
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        socialSecurityNumberColumn.setMinWidth(200);

        diagnosisColumn = new TableColumn<>("Diagnosis");
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        diagnosisColumn.setVisible(false);

        generalPractitionerColumn = new TableColumn<>("General Practitioner");
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        generalPractitionerColumn.setVisible(false);

        patientsTable.getColumns().addAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn, diagnosisColumn, generalPractitionerColumn);

        return patientsTable;
    }

}
