package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * Class GUIObjectFactoryClient
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class GUIObjectFactoryClient extends Application {

    private Stage stage;


    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Patient Register");

        GUIObject topMenu = GUIObjectFactory.getGUIObject("TOPMENU");

        GUIObject toolBar = GUIObjectFactory.getGUIObject("TOOLBAR");

        GUIObject patientsTable = GUIObjectFactory.getGUIObject("TABLE");

        GUIObject statusBar = GUIObjectFactory.getGUIObject("STATUSBAR");

        VBox centerPane = new VBox(toolBar.build(), patientsTable.build(), statusBar.build());
        centerPane.setSpacing(10);

        BorderPane layout = new BorderPane();
        layout.setTop(topMenu.build());
        layout.setCenter(centerPane);

        Scene scene = new Scene(layout, 600, 500);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}
