package idatt2001.mappeDel2.patients.factoryPattern;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

/**
 * Class DetailsForm
 * @version 1.01 2021-05-04
 * @author Hadar Hayat
 */
public class Toolbar implements GUIObject{

    private HBox toolContainer;
    private Button addButton;
    private Button editButton;
    private Button removeButton;

    @Override
    /**
     * Build method from GUIObject Interface
     * Builds the toolbar with the buttons to act on patient register
     */
    public Node build(){

        addButton = new Button();
        SVGPath svgAdd = new SVGPath();
        svgAdd.setContent("M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z");
        addButton.setStyle("-fx-background-color: green;");
        addButton.setMinWidth(20);
        addButton.setShape(svgAdd);

        editButton = new Button();
        SVGPath svgEdit = new SVGPath();
        svgEdit.setContent("M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z");
        editButton.setStyle("-fx-background-color: blue;");
        editButton.setMinWidth(20);
        editButton.setShape(svgEdit);

        removeButton = new Button();
        SVGPath svgRemove = new SVGPath();
        svgRemove.setContent("M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z");
        removeButton.setStyle("-fx-background-color: red;");
        removeButton.setMinWidth(20);
        removeButton.setShape(svgRemove);

        toolContainer = new HBox();
        toolContainer.getChildren().addAll(addButton, editButton, removeButton);
        toolContainer.setSpacing(50);
        toolContainer.setPadding(new Insets(20));
        return toolContainer;
    }

}
