package idatt2001.mappeDel2.patients.backend;

import idatt2001.mappeDel2.patients.controllers.PrimaryController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Collection;

public class PatientRegister {

    /**
     * Class PatientRegister
     * @version 1.01 2021-04-30
     * @author Hadar Hayat
     */
    private ArrayList<Patient> patients;
    private static PatientRegister patientsInstance = new PatientRegister();
    private PrimaryController primaryController;

    /**
     * Constructor for class PatientRegister
     * Creates a new PatientRegister
     * No parameters required
     *
     */
    public PatientRegister(){
        patients = new ArrayList<Patient>();
    }

    /**
     * Return the list of patient
     *
     * @return patients
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Return an instance of this class
     *
     * @return patientsInstance
     */
    public static PatientRegister getPatientsInstance() {
        return patientsInstance;
    }

    /**
     * Update/set the instance for this class
     *
     * @param patientsInstance
     */
    public static void setPatientsInstance(PatientRegister patientsInstance) {
        PatientRegister.patientsInstance = patientsInstance;
    }

    /**
     * Return the controller for the primary fxml file for the application
     *
     * @return primaryController
     */
    public PrimaryController getPrimaryController() {
        return primaryController;
    }

    /**
     * Update/set the controller for the primary fxml file for the application
     *
     * @param primaryController
     */
    public void setPrimaryController(PrimaryController primaryController) {
        this.primaryController = primaryController;
    }

    /**
     * Return the patients list as an observable list
     * Needed to display the patients in a table
     *
     * @return FXCollections.observableArrayList(patients);
     */
    public ObservableList<Patient> getAsObservableList(){
        return FXCollections.observableArrayList(patients);

    }

    /**
     * Check tf the input patient already exists in the patients list
     * If it exists, return false
     * If it does not exist, add input patient to the patients list and return true
     *
     * @param patientInput
     * @return true or false
     */
    public boolean addPatient(Patient patientInput) throws IllegalArgumentException{
        if(patientInput != null && !patients.contains(patientInput)){
            patients.add(patientInput);
            return true;
        }throw new IllegalArgumentException("The patient you are trying to add, already exist.");
    }

    /**
     * Check tf the input patient to edit exists in the patients list
     * If it exists, update the patient to edit with the patient edit details and return true
     * If it does not exist, return false
     *
     * @param patientToEdit
     * @param patientEdit
     * @return true or false
     */
    public boolean editPatient(Patient patientToEdit, Patient patientEdit) throws IllegalArgumentException{

        if(patients.contains(patientToEdit)){
            patientToEdit.setFirstName(patientEdit.getFirstName());
            patientToEdit.setLastName(patientEdit.getLastName());
            patientToEdit.setSocialSecurityNumber(patientEdit.getSocialSecurityNumber());
            patientToEdit.setDiagnosis(patientEdit.getDiagnosis());
            patientToEdit.setGeneralPractitioner(patientEdit.getGeneralPractitioner());
            return true;
        }else {
            throw new IllegalArgumentException("The patient you are trying to edit, does not exist.");
        }
    }

    /**
     * Check tf the input patient to delete exists in the patients list
     * If it exists, remove the input patient from the patients list and return true
     * If it does not exist, return false
     *
     * @param patientInput
     * @return true or false
     */
    public boolean removePatient(Patient patientInput) throws IllegalArgumentException{
        if(patientInput != null && patients.contains(patientInput)){
            patients.remove(patientInput);;
            return true;
        }else{
            throw new IllegalArgumentException("The patient you are trying to remove does not exist.");
        }
    }
}
