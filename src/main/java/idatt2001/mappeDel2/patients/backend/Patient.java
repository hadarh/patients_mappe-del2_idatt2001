package idatt2001.mappeDel2.patients.backend;

/**
 * Class Patient
 * @version 1.01 2021-04-30
 * @author Hadar Hayat
 */
public class Patient {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor for class Patient
     * Creates a new Patient object
     * Including all required parameters and integrated exception for missing required information about patient
     *
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * @param diagnosis
     * @param generalPractitioner
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Return the first name of the patient
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Update/set the first name of the patient
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Return the last name of the patient
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Update/set the last name of the patient
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Return the social security number of the patient
     *
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Update/set the social security number of the patient
     *
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Return the diagnosis of the patient
     *
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }


    /**
     * Update/set the diagnosis of the patient
     *
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Return the general practitioner of the patient
     *
     * @return generalPractitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Update/set the general practitioner of the patient
     *
     * @param generalPractitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
