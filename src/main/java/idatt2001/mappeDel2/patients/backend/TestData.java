package idatt2001.mappeDel2.patients.backend;

import idatt2001.mappeDel2.patients.backend.Patient;
import idatt2001.mappeDel2.patients.backend.PatientRegister;

public class TestData {

    public static void generate(){
        PatientRegister.getPatientsInstance().getPatients().add(new Patient("Ole", "Olsen", "12345678910", "Covid-19", "Per Peterson"));
        PatientRegister.getPatientsInstance().getPatients().add(new Patient("Nils", "Nilsen", "10987654321", "Ebola", "Karl Peterson"));
        PatientRegister.getPatientsInstance().getPatients().add(new Patient("Karl", "Karlsen", "11111111111", "Cancer", "Ole Peterson"));
    }
}
